from time import perf_counter


class callstat:
    """
    Декоратор, подсчитывающий количество вызовов и 
    среднюю длительность вызова задекорированной функции.

    Пример использования:

    @callstat
    def add(a, b):
        return a + b

    >>> add.call_count
    0
    >>> add(1, 2)
    3
    >>> add.call_count
    1

    Подсказки по реализации: функторы, @property
    Для измерения времени выполнения - perf_counter, см. импорт.

    """
    def call_counter(func):
        def helper(x):
            helper.calls += 1
            return func(x)

        helper.calls = 0
        return helper

    @property
    def succ(x):
        return x + 1

foo = callstat()
